# MAC110 - MiniEP7
# <Eduardo Ribeiro Silva de Oliveira> - <11796920>

# <funções da parte 1>
function sin(x)
        somatoria = x
        n = 1
        while(n < 10)
                somatoria += big(((pot(-1,n) * pot(x,2n+1)) / fatorial(2n+1)))
                n += 1
        end
        return somatoria
end

function cos(x)
        somatoria = 1
        n = 1
        while(n < 10)
                somatoria += big(((pot(-1,n) * pot(x,2n)) / fatorial(2n)))
                n += 1
        end
        return somatoria
end

function tan(x)
        somatoria = x
        n = 2
        while(n < 10)
                somatoria += big(((pot(4,n) * (pot(4,n) - 1) * bernoulli(n) * pot(x,2n-1)) / fatorial(2n)))
                n += 1
        end
        return somatoria
end

function fatorial(n)
        f = big(1)
        for i = 1 : n
                f = f *i
        end
        return f
end

function pot(valor, potencia)
        res = big(1)
        for i=1 : potencia
                res = res * valor
        end
        return res
end

function bernoulli(n)
        n *= 2
        A = Vector{Rational{BigInt}}(undef, n + 1)
        for m = 0 : n
                A[m + 1] = 1 // (m + 1)
                for j = m : -1 : 1
                        A[j] = j * (A[j] - A[j + 1])
                end
        end
        return big(abs(A[1]))

end

# <funções da parte 2>

function quaseigual(v1,v2)
        erro = 0.0001
        if (abs(v1-v2) <= erro)
                return true
        else
                return false
        end
end

function check_sin(value, x)
        if quaseigual(value, sin(x))
                return true
        else
                return false
        end
end

function check_cos(value,x)
        if quaseigual(value, cos(x))
                return true
        else
                return false
        end
end

function check_tan(value, x)
        if x > pi/2
                if quaseigual(value,tan(x))
                        return true
                else
                        return false
                end
        end
        if quaseigual(value, tan(x))
                return true
        else
                return false
        end
end

function test()
        valor = pi/3
        println("O valor parametrizado é ", valor)
        print("Sin: ")
        println(sin(valor))
        print("Cos: ")
        println(cos(valor))
        print("Tan: ")
        println(tan(valor))
        println("---------------------")
        println("sin(pi/6) = 0.5?")
        println(check_sin(0.5, pi/6))
        println("cos(pi/3) = 0.5?")
        println(check_cos(0.5, pi/3))
        println("tan(pi) = 0?")
        println(check_tan(0, pi))
        println("sin(pi) = 42?")
        println(check_sin(42, pi/4))
        println("cos(pi) = 42?")
        println(check_cos(42, pi/4))
        println("tan(pi/4) = 42?")
        println(check_tan(42, pi/4))
        println("--------------------")
end

function taylor_sin(x)
        somatoria = x
        n = 1
        while(n < 10)
                somatoria += big(((pot(-1,n) * pot(x,2n+1)) / fatorial(2n+1)))
                n += 1
        end
        return somatoria
end

function taylor_cos(x)
        somatoria = 1
        n = 1
        while(n < 10)
                somatoria += big(((pot(-1,n) * pot(x,2n)) / fatorial(2n)))
                n += 1
        end
        return somatoria
end

function taylor_tan(x)
        somatoria = x
        n = 2
        while(n < 10)
                somatoria += big(((pot(4,n) * (pot(4,n) - 1) * bernoulli(n) * pot(x,2n-1)) / fatorial(2n)))
                n += 1
        end
        return somatoria
end

test()
